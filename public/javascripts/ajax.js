window.ajax = (function () {
    function get(url, callback) {
        request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseText);
            }
        }
        request.open('GET', url, true);
        request.send();
    }

    function post(data, url, callback) {
        request = new XMLHttpRequest();
        var body = JSON.stringify(data);

        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseText);
            }
        }
        request.open('POST', url, true);
        request.setRequestHeader('Accept', 'application/json');
        request.setRequestHeader('Content-Type', 'application/json');
        request.send(body);
    }

    return {
        get: get,
        post: post
    }
})();