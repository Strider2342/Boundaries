"use strict";

module.exports = function (router) 
{
  var ctrlMap 		= require('../controllers/ctrl-map');
  
  router.route('/').get(ctrlMap.get);
  router.route('/').post(ctrlMap.add);
  
  return router;
};