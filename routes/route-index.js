"use strict";

module.exports = function (router) 
{
  var ctrlIndex 		= require('../controllers/ctrl-index');
  
  router.route('/').all(ctrlIndex.show);
  
  return router;
};