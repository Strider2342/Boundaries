"use strict";

module.exports = function (express, app)
{
	var router = express.Router();
	
	var routerIndex		= require('./route-index')(express.Router());
    var routerMap		= require('./route-map')(express.Router());
	
	app.use('/'			, routerIndex);
    app.use('/map'      , routerMap);
};