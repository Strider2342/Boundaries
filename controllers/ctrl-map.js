"use strict";

var fs = require('fs');
var path = require('path');

var Boundary = require("../models/model-boundaries").Boundary;

var ctrl = {
	"get" : function(req, res, next) {
        Boundary.find(function(error, docs) {
            if (error) {
                res.send({ "success" : false, "error" : error });
            } else {
                if (docs) {
                    res.send({ "success" : true, "boundaries" : docs });
                } else {
                    res.send({ "success" : false, "error" : "No entry in database" });
                }
            }
        })
	}
,   "add" : function(req, res, next) {
        var data = req.body;
        var boundary = Boundary(data);
        
        console.log(boundary);

        Boundary.findOneAndUpdate({name: data.name}, boundary, function(error, doc) {
            if (error) {
                console.log("Error: " + error);
                res.send({ "success" : false, "error" : error });
            } else {
                console.log("Data inserted");
                res.send({ "success" : true });
            }
        })
    }
,   "uploadFile" : function(req, res, next) {
        if (req.busboy) {
            req.pipe(req.busboy);

            req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                var path = process.env.ROOTDIR + "/resources/uploaded/"
                var fstream = fs.createWriteStream(path + filename);

                file.pipe(fstream);

                fstream.on('close', function() {
                    fs.readFile(path + filename, function (error, data) {
                        if (error) {
                            res.send({ success: false, error: error });
                        } else {
                            res.send({ success: true });
                        }
                    });
                });
            });
        } else {
            res.send("Busboy not initialized");
        }
    }
,   "fromFile" : function(req, res, next) {
        var filePath = path.join(process.env.ROOTDIR, 'resources/uploaded/map1.json');

        fs.readFile(filePath, 'utf8', function(error, contents) {
            if (error) {
                res.send({ success: false, error: "File not found" });
            } else {
                res.send({ "success" : true, "paths" : contents });
            }
        });
    }
}

Object.keys(ctrl).forEach(function(key) {
	exports[key] = ctrl[key];
});