"use strict;"

var mongoose = require('mongoose');

var boundarySchema = new mongoose.Schema({
    name        : { type: String, required: true },
    center      : {
        lat     : { type: Number, required: true },
        lng     : { type: Number, required: true }
    },
    zoom        : { type: Number, required: true },
    boundaries  : [{
        start   : { type: Date, required: true },
        end     : { type: Date, required: false },
        path    : [{
            lng     : { type: Number, required: true },
            lat     : { type: Number, required: true }
        }]
    }]
}, {collection: "Boundaries", strict: false });

var Boundary = mongoose.model('Boundarie', boundarySchema);

exports.Boundary = Boundary;